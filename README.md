# Gitlab kubernetes agent config

* [Agent registration](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#register-the-agent-with-gitlab)
* [Agent authorization](https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_tunnel.html#authorize-the-agent-to-access-your-projects)
* [Review apps](https://docs.gitlab.com/ee/ci/review_apps/index.html#review-apps-examples)
* [gitlab-agent repo and helm chart](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent)

## Epics

* [CI/CD tunnel](https://gitlab.com/groups/gitlab-org/-/epics/5528)
* [GitLab Agent for Kubernetes](https://gitlab.com/groups/gitlab-org/-/epics/3329)

## Issues

* [Authorization not working](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/issues/243)

* [Gitlab Agent Feedback issue](https://gitlab.com/gitlab-org/gitlab/-/issues/342696)
* [Docs: Kubernetes contexts from agent not injected into pipeline of projects authorized to use the agent](https://gitlab.com/gitlab-org/gitlab/-/issues/346636)
* [KAS CI/CD Tunnel ci_access does not work for projects/groups outside of the group with the agent config project](https://gitlab.com/gitlab-org/gitlab/-/issues/346566)
